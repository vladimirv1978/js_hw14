let head = document.querySelector("head");
let tagOldStyle = '<link id="oldStyle" rel="stylesheet" href="./Style/style.css" />';
let tagNewStyle = '<link id="newStyle" rel="stylesheet" href="./Style/styleNew.css" />';

//-------------------создаем кнопку-----------------------------------
let header = document.querySelector(".header_logo");
header.insertAdjacentHTML("beforeend",'<input class="header_button" type="button" value="Змінити тему">');
//--------------------------------------------------------------------

//--------------------загружаем страницу и проверяем localStorage------
function pageStart() {
  switch (localStorage.getItem("Style")) {

    case null:
      localStorage.setItem("Style",tagOldStyle);
      head.insertAdjacentHTML("beforeend", localStorage.getItem("Style"));
      break;
    case tagOldStyle:
      head.insertAdjacentHTML("beforeend", localStorage.getItem("Style"));
      break;
    case tagNewStyle:
      head.insertAdjacentHTML("beforeend", localStorage.getItem("Style"));
    default:
      break;
  }
  buttonListener();
}
//-----------------------------------------------------------------------------

function buttonListener() {
    document.querySelector(".header_button").addEventListener("click", changeStyle);
}

function changeStyle(){
    let oldStyle = document.querySelector("#oldStyle");
    let newStyle = document.querySelector("#newStyle");
    if (oldStyle) {
        oldStyle.remove();
        head.insertAdjacentHTML("beforeend",tagNewStyle);
        localStorage.setItem("Style",tagNewStyle);
      }
    if (newStyle) {
        newStyle.remove();
        head.insertAdjacentHTML("beforeend", tagOldStyle);
        localStorage.setItem("Style", tagOldStyle);
    }
}

pageStart();


